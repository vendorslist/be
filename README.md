# Api .NET Core 6 API 
### Setup Project 

* open Package Manager Console: 
```
Update-Database
```

* if no migration added then run: 
```
Add-Migration InitialSetup 
```

* open powershell, then run: 
```
cd Api && dotnet run seeddata
```

* * The seeder will generate 2 following vendor data that can be used as login:
```
username: John
password: 123!@#qwe

username: Doe
password: 123!@#qwe
```