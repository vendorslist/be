using Api.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "MyPolicy",
        policy =>
        {
            policy.WithOrigins("http://localhost:8080")
                    .AllowAnyHeader() 
                    .AllowAnyMethod();
                /*.WithMethods("PUT", "DELETE", "GET", "POST");*/
        });
});

var connString = builder.Configuration.GetConnectionString("pg_db_con");
builder.Services.AddTransient<DataSeeder>();
builder.Services.AddDbContext<VendorDbContext>(x => x.UseNpgsql(connString));

var app = builder.Build();

if (args.Length == 1 && args[0].ToLower() == "seeddata")
{
    SeedData(app);
}

void SeedData(IHost app)
{
    var scopeFactory = app.Services.GetService<IServiceScopeFactory>();

    using(var scope = scopeFactory.CreateScope())
    {
        var serv = scope.ServiceProvider.GetService<DataSeeder>();
        serv.Seed();
    }
}

app.MapGet("/", (Func<string>)(() => "Hello World"));

app.MapGet("/vendor/{id}", ([FromServices] VendorDbContext dbContext, int id) =>
    {
        return dbContext.Vendors.Where(x => x.id == id).FirstOrDefault();
    }
);

app.MapGet("/vendors", ([FromServices] VendorDbContext dbContext) =>
    {
        return dbContext.Vendors.ToList();
    }
);

app.MapPut("/vendor/{id}", ([FromServices] VendorDbContext dbContext, Vendor vendor) =>
    {
        if (vendor.password == null)
        {
            vendor.password = "123!@#qwe";
        }
        dbContext.Vendors.Update(vendor);
        dbContext.SaveChanges();
        return dbContext.Vendors.Where(x => x.id == vendor.id).FirstOrDefault();
    }
);

app.MapPost("/vendor", ([FromServices] VendorDbContext dbContext, Vendor vendor) =>
    {
        if (vendor.password == null)
        {
            vendor.password = "123!@#qwe";
        }
        dbContext.Vendors.Add(vendor);
        dbContext.SaveChanges();
        return dbContext.Vendors.ToList();
    }
);

app.MapPost("/vendor/login", ([FromServices] VendorDbContext dbContext, Vendor vendor) =>
    {
        return dbContext.Vendors.Where(x => x.name == vendor.name).Where(x => x.password == vendor.password).FirstOrDefault();
    }
);

app.MapDelete("/vendor/{id}", async (int id, VendorDbContext dbContext) =>
{
    if (await dbContext.Vendors.FindAsync(id) is Vendor vendor)
    {
        dbContext.Vendors.Remove(vendor);
        await dbContext.SaveChangesAsync();
        return Results.Ok(vendor);
    }

    return Results.NotFound();
});

app.UseCors("MyPolicy");
app.Run();