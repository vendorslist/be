﻿using Microsoft.EntityFrameworkCore;

namespace Api.Model
{
    public class VendorDbContext : DbContext
    {
        public VendorDbContext()
        {
        }

        public VendorDbContext(DbContextOptions<VendorDbContext> options) : base(options)
        {
        }

        public DbSet<Vendor> Vendors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var conString = configuration.GetConnectionString("pg_db_con");
            optionsBuilder.UseNpgsql(conString);
        }
    }
}
