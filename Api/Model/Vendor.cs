﻿namespace Api.Model
{
    public class Vendor
    {
        public int id { get; set; }
        public string vendor_id { get; set; }
        public string name { get; set; }
        public string password { get; set; }
    }
}
