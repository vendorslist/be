﻿namespace Api.Model
{
    public class DataSeeder
    {
        private readonly VendorDbContext vendorDbContext;

        public DataSeeder(VendorDbContext vendorDbContext)
        {
            this.vendorDbContext = vendorDbContext;
        }

        public void Seed()
        {
            if(!vendorDbContext.Vendors.Any())
            {
                var vendor = new List<Vendor>()
                {
                    new Vendor()
                    {
                        vendor_id = "V0001",
                        name = "John",
                        password = "123!@#qwe"
                    },
                    new Vendor()
                    {
                        vendor_id = "V0002",
                        name = "Doe",
                        password = "123!@#qwe"
                    }
                };

                vendorDbContext.Vendors.AddRange(vendor);
                vendorDbContext.SaveChanges();
            }
        }
    }
}
